#! /bin/sh

set -xeu

INDEX_PATH="/usr/share/nginx/html/devfiles/index.json"

TMPFILE="$(mktemp)"
cat "${INDEX_PATH}" > "${TMPFILE}"

envsubst < "${TMPFILE}" | cat > "${INDEX_PATH}"

rm -f "${TMPFILE}"

nginx "-g" "daemon off;"
