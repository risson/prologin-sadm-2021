{
  description = "Prologin NixOS configuration for 2021 finale";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-21.05";
    nixpkgs-master.url = "github:NixOS/nixpkgs/master";
    nixpie.url = "git+https://gitlab.cri.epita.fr/cri/infrastructure/nixpie.git";
    futils.url = "github:numtide/flake-utils";
    sops-nix.url = "github:Mic92/sops-nix";
    deploy-rs.url = "github:serokell/deploy-rs";
    impermanence.url = "github:nix-community/impermanence";
  };

  outputs =
    { self
    , nixpkgs
    , nixpkgs-master
    , nixpie
    , futils
    , sops-nix
    , deploy-rs
    , impermanence
    } @ inputs:
    let
      inherit (nixpkgs) lib;
      inherit (lib) recursiveUpdate;
      inherit (futils.lib) eachDefaultSystem;

      pkgImport = pkgs: system: withOverrides:
        import pkgs {
          inherit system;
          config = {
            allowUnfree = true;
          };
          overlays = (lib.attrValues self.overlays);
        };

      pkgset = system: {
        pkgs = pkgImport nixpkgs system true;
        pkgsMaster = pkgImport nixpkgs-master system false;
      };

      anySystemOutputs = {
        lib = import ./lib { inherit lib; };

        overlays = (import ./overlays) // {
          packages = import ./pkgs;
        };

        nixosModules = (import ./modules) // {
          profiles = import ./profiles;
          sadm = import ./modules/sadm.nix;
        };

        nixosConfigurations =
          let
            system = "x86_64-linux";
          in
          (import ./hosts (
            recursiveUpdate inputs {
              inherit lib system;
              pkgset = pkgset system;
            }
          )) // (import ./images (
            recursiveUpdate inputs {
              inherit lib system;
              pkgset = pkgset system;
            }
          ));

        deploy = {
          nodes =
            builtins.mapAttrs
              (n: v: {
                hostname = v.config.deploy.hostname;
                profiles.system = {
                  sshUser = "root";
                  user = "root";
                  autoRollback = false;
                  magicRollback = false;
                  fastConnection = true;
                  sshOpts = [ "-J" "fw-cri" ];
                  path = deploy-rs.lib.x86_64-linux.activate.nixos v;
                };
              })
              (lib.filterAttrs (name: value: !value.config.netboot.enable) self.nixosConfigurations);
        };

        checks = builtins.mapAttrs (system: deployLib: deployLib.deployChecks self.deploy) deploy-rs.lib;
      };

      multiSystemOutputs = eachDefaultSystem (system:
        let
          inherit (pkgset system) pkgs pkgsMaster;
        in
        {
          devShell = pkgs.mkShell {
            sopsPGPKeyDirs = [
              "./vars/sops-keys/hosts"
              "./vars/sops-keys/users"
            ];

            nativeBuildInputs = [
              sops-nix.packages.${system}.sops-pgp-hook
            ];

            buildInputs = with pkgs; [
              git
              sops
              sops-nix.packages.${system}.ssh-to-pgp
              nixpkgs-fmt
              pre-commit
              kubernetes-helm
              kubectl
              stern
              kubectx
              deploy-rs.packages.${system}.deploy-rs
              poetry
            ];
          };

          packages = self.lib.overlaysToPkgs self.overlays pkgs;
        });
    in
    recursiveUpdate multiSystemOutputs anySystemOutputs;
}
