{ config, lib, pkgs, modulesPath, ... }:

{
  imports = [
    (modulesPath + "/installer/scan/not-detected.nix")
  ];

  boot = {
    loader = {
      grub = {
        enable = true;
        version = 2;
        devices = [ "/dev/sda" "/dev/sdb" ];
      };
    };

    initrd = {
      availableKernelModules = [
        "uhci_hcd"
        "ehci_pci"
        "hpsa"
        "usbhid"
        "sd_mod"
      ];
      kernelModules = [ "dm-snapshot" ];
    };

    kernelModules = [ "kvm-intel" ];
  };

  fileSystems = {
    "/" = {
      device = "/dev/disk/by-uuid/b119a751-e5a5-483e-ba83-f111d7f81a69";
      fsType = "ext4";
    };
  };

  swapDevices = [
    { device = "/dev/disk/by-uuid/77321143-5e37-4258-91f6-815a17e1ce6c"; }
    { device = "/dev/disk/by-uuid/0983f080-94ba-4f02-83e4-ec3f1b003758"; }
  ];

  powerManagement.cpuFreqGovernor = "performance";
  nix.maxJobs = 8;
}
