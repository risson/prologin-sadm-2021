{ config, lib, pkgs, modulesPath, ... }:

{
  imports = [
    (modulesPath + "/installer/scan/not-detected.nix")
  ];

  boot = {
    loader = {
      systemd-boot.enable = true;
      efi.canTouchEfiVariables = true;
    };

    initrd = {
      availableKernelModules = [
        "ahci"
        "nvme"
        "rtsx_pci_sdmmc"
        "sd_mod"
        "usb_storage"
        "usbhid"
        "xhci_pci"
      ];
    };

    kernelModules = [ "kvm-intel" ];
  };

  fileSystems = {
    "/" = {
      device = "/dev/disk/by-uuid/a1b4c73a-f58a-4b9e-baa9-d24f4449f4cc";
      fsType = "ext4";
    };

    "/boot" = {
      device = "/dev/disk/by-uuid/0AA8-75D2";
      fsType = "vfat";
    };
  };

  swapDevices = [
    { device = "/dev/disk/by-uuid/40c2a142-0dab-485a-ba3a-b05984e912ca"; }
  ];

  powerManagement.cpuFreqGovernor = "performance";
}
