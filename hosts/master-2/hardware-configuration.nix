{ config, lib, pkgs, modulesPath, ... }:

{
  imports = [
    (modulesPath + "/installer/scan/not-detected.nix")
  ];

  boot = {
    loader = {
      systemd-boot.enable = true;
      efi.canTouchEfiVariables = true;
    };

    initrd = {
      availableKernelModules = [
        "ahci"
        "nvme"
        "rtsx_pci_sdmmc"
        "sd_mod"
        "usb_storage"
        "usbhid"
        "xhci_pci"
      ];
    };

    kernelModules = [ "kvm-intel" ];
  };

  fileSystems = {
    "/" = {
      device = "/dev/disk/by-uuid/831d4967-dc30-4409-afec-f879d8d37c13";
      fsType = "ext4";
    };

    "/boot" = {
      device = "/dev/disk/by-uuid/BC6C-BBFB";
      fsType = "vfat";
    };
  };

  swapDevices = [
    { device = "/dev/disk/by-uuid/f5d94f84-da4a-4c5c-8257-4cdee7d9f2e5"; }
  ];

  powerManagement.cpuFreqGovernor = "performance";
}
