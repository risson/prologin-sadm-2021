{ config, lib, pkgs, modulesPath, ... }:

{
  imports = [
    (modulesPath + "/installer/scan/not-detected.nix")
  ];

  boot = {
    loader = {
      systemd-boot.enable = true;
      efi.canTouchEfiVariables = true;
    };

    initrd = {
      availableKernelModules = [
        "ahci"
        "nvme"
        "rtsx_pci_sdmmc"
        "sd_mod"
        "usb_storage"
        "usbhid"
        "xhci_pci"
      ];
    };

    kernelModules = [ "kvm-intel" ];
  };

  fileSystems = {
    "/" = {
      device = "/dev/disk/by-uuid/8597b30c-9fc6-4737-bceb-b55617923119";
      fsType = "ext4";
    };

    "/boot" = {
      device = "/dev/disk/by-uuid/B9D2-5C34";
      fsType = "vfat";
    };
  };

  swapDevices = [
    { device = "/dev/disk/by-uuid/653d8196-f4a8-42ca-89c8-fb6605ed77b4"; }
  ];

  powerManagement.cpuFreqGovernor = "performance";
}
