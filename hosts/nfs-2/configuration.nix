{ inputs, lib, pkgs, ... }:

{
  imports = [
    inputs.self.nixosModules.profiles.server

    ./hardware-configuration.nix
  ];

  networking = {
    hostName = lib.mkOverride 10 "nfs-2";
    useDHCP = false;
    nameservers = [ "10.224.4.2" ];

    interfaces.eno1 = {
      ipv4.addresses = [{
        address = "10.224.33.242";
        prefixLength = 24;
      }];
    };

    defaultGateway = {
      address = "10.224.33.1";
      interface = "eno1";
    };
  };

  users.users.root.openssh.authorizedKeys.keys = [
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAINlovNp4ufIs0NcdyJqTjHmW2/Uj9QfS9hf/fX+/yphT rsnapshot@nfs-2"
  ];

  networking.firewall = {
    allowedTCPPorts = [ 111 2049 4505 4506 ];
    allowedUDPPorts = [ 111 2049 4505 4506 ];
  };
  services.nfs.server = {
    enable = true;
    createMountPoints = true;
    exports = ''
      /srv/nfs    10.224.32.0/24(rw,sync,no_subtree_check,no_root_squash) 10.224.33.0/24(rw,sync,no_subtree_check,no_root_squash) 10.224.34.0/24(rw,sync,no_subtree_check,no_root_squash)
    '';
  };

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "21.05"; # Did you read the comment?
}
