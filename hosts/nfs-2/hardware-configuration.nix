{ config, lib, pkgs, modulesPath, ... }:

{
  imports = [
    (modulesPath + "/installer/scan/not-detected.nix")
  ];

  boot = {
    loader = {
      grub = {
        enable = true;
        version = 2;
        devices = [ "/dev/sda" "/dev/sdb" ];
      };
    };

    initrd = {
      availableKernelModules = [
        "xhci_pci"
        "ehci_pci"
        "ahci"
        "usbhid"
        "sd_mod"
      ];
      kernelModules = [ "dm-snapshot" ];
    };

    kernelModules = [ "kvm-intel" ];
  };

  fileSystems = {
    "/" = {
      device = "/dev/disk/by-uuid/68481e66-94a4-49a7-b6bf-9a4bceddf62c";
      fsType = "ext4";
    };
  };

  swapDevices = [
    { device = "/dev/disk/by-uuid/e74aa4ac-a819-4e87-b19f-6dcab537c7e1"; }
    { device = "/dev/disk/by-uuid/72ae5edd-37bd-46f0-8c47-7844543dfa85"; }
  ];

  powerManagement.cpuFreqGovernor = "performance";
  nix.maxJobs = 8;
}
