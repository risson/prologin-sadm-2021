{ inputs, lib, pkgs, ... }:

{
  imports = [
    inputs.self.nixosModules.profiles.server
    inputs.self.nixosModules.profiles.nuc

    ./hardware-configuration.nix
  ];

  networking = {
    useDHCP = false;
    interfaces.eno1.useDHCP = true;
  };

  services.rsnapshot = {
    enable = true;
    cronIntervals = {
      alpha = "42 * * * *";
      beta = "52 */6 * * *";
      gamma = "02 */12 * * *";
    };
    extraConfig = ''
      snapshot_root		/srv/snapshots/

      retain		alpha		12
      retain		beta		8
      retain		gamma		100

      backup		root@10.224.33.241:/srv/		concours/
      backup		root@10.224.33.242:/srv/		nfs-2/
    '';
  };

  networking.firewall = {
    allowedTCPPorts = [ 4505 4506 8001 8002 ];
  };
  virtualisation.oci-containers = {
    containers = {
      dragonflyoss = {
        image = "registry.gitlab.com/risson/prologin-sadm-2021/dragonflyoss/supernode:0.3.1";
        imageFile = pkgs.dockerTools.pullImage {
          imageName = "registry.gitlab.com/risson/prologin-sadm-2021/dragonflyoss/supernode";
          imageDigest = "sha256:ec08ed679cd165a99a2f08b62aafd7a0952d1715769f973c836fbe685a010d83";
          finalImageTag = "0.3.1";
          sha256 = "sha256-M6KZoP6BCRP9H98Oh6ij8+pPODxbEEWubaJL42d/JzM=";
        };
        cmd = [ "-Dsupernode.advertiseIp=10.224.33.2" ];
        ports = [ "8001" "8002" ];
        volumes = [ "/srv/dragonfly:/home/admin/supernode/" ];
        extraOptions = [ "--network=host" ];
      };
    };
  };

  services.salt.master = {
    enable = true;
    configuration = {
      auto_accept = true;
    };
  };

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "21.05"; # Did you read the comment?
}
