{ lib, ... }:

with lib;

{
  options = {
    deploy = mkOption {
      type = types.attrs;
    };
  };
}
