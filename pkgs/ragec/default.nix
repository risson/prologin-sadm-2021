{ lib, poetry2nix }:

poetry2nix.mkPoetryApplication {
  projectDir = ./.;
  src = ./.;

  overrides = poetry2nix.overrides.withDefaults (self: super: {
    pycountry-convert = super.pycountry-convert.overridePythonAttrs (old: {
      buildInputs = [ super.pytest-runner ];
      doCheck = false;
    });
  });

  meta = with lib; {
    maintainers = with maintainers; [ risson ];
  };
}
