{ config, pkgs, lib, ... }:

{
  services.keepalived = {
    enable = true;
    vrrpScripts = {
      "check_haproxy" = {
        script = "${pkgs.killall}/bin/killall -0 haproxy";
        user = "root";
        interval = 3;
        fall = 10;
        rise = 2;
      };
    };
    vrrpInstances."VI_02" = {
      state = "BACKUP";
      interface = "eno1";
      virtualRouterId = 248;
      noPreempt = true;
      priority = 100;
      virtualIps = [{
        addr = "10.224.33.248";
      }];
      trackScripts = [
        "check_haproxy"
      ];
      trackInterfaces = [ "eno1" ];
    };
  };

  networking.firewall.extraCommands = ''
    iptables -A INPUT -p vrrp -j ACCEPT
  '';

  networking.firewall.extraStopCommands = ''
    iptables -D INPUT -p vrrp -j ACCEPT
  '';

  services.haproxy = {
    enable = true;
    config = ''
        # global configuration is above
        log /dev/log local0
        log /dev/log local1 notice

      defaults
        mode tcp
        log global
        option tcplog
        option dontlognull
        option forwardfor except 127.0.0.0/8
        option redispatch
        retries 1
        timeout queue 20s
        timeout connect 5s
        timeout client 20s
        timeout server 20s
        timeout check 10s

      backend api-server
        option httpchk GET /healthz
        http-check expect status 200
        mode tcp
        option ssl-hello-chk
        balance roundrobin

        server master-1-apiserver 10.224.33.5:6443 check
        server master-2-apiserver 10.224.33.6:6443 check
        server master-3-apiserver 10.224.33.7:6443 check

      frontend api-server
        bind 10.224.33.248:6443
        mode tcp
        option tcplog
        use_backend api-server
    '';
  };
}
