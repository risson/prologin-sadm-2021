{
  core = ./core;
  server = ./server;
  gate = ./gate;
  nuc = ./nuc;

  k3sNode = ./k3s/node;
  k3sMaster = ./k3s/master;
  k3sWorker = ./k3s/worker;
}
